from flask import Flask
import os.path
from flask_wtf.csrf import CsrfProtect

app=Flask(__name__)
app.debug=True

from flask.ext.script import Manager
manager=Manager(app)
csrf = CsrfProtect()
csrf.init_app(app)

def mkpath(p):
	return os.path.normpath(
		os.path.join(
			os.path.dirname(__file__),p))

from flask.ext.sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI']=(
	'sqlite:///'+mkpath('../myapp.db'))
db=SQLAlchemy(app)

app.config['BOOTSTRAP_SERVE_LOCAL']=True

from flask.ext.bootstrap import Bootstrap
Bootstrap(app)

from flask.ext.login import LoginManager
login_manager=LoginManager(app)
login_manager.login_view="login"

app.config['SECRET_KEY']="58b4ac69-c9cf-49ae-a1f0-de33fd71d1fa"
