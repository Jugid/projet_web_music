from .app import manager, db
from .models import get_genre,get_albums_spec,add_genre,get_all_genre,add_genre,get_user_spec

@manager.command
def loaddb(filename):
	'''Creates the tables and populates them with data'''
	db.create_all()
	import yaml
	albums=yaml.load(open(filename))
	from .models import Chanteur,Album,Genre,appartenir,contient
	# Ajout des chanteurs
	chanteur={}
	for b in albums:
		a=b["by"]
		if a not in chanteur:
			o=Chanteur(nom=a)
			db.session.add(o)
			chanteur[a]=o
	db.session.commit()

	genres=set()
	for c in albums:
		a=c["genre"]
		for genre in a :
			if genre not in genres:
				o=Genre(nom=genre)
				genres.add(o.nom)
				db.session.add(o)
	db.session.commit()

	#Ajout des albums
	for b in albums:
		a=chanteur[b["by"]]
		o=Album(nom=b["title"],
			annee=b["releaseYear"],
			img=b["img"],
			idChanteur=a.id)
		for gen in b["genre"]:
			g=get_genre(gen)
			o.genre.append(g)
		db.session.add(o)
	db.session.commit()

@manager.command
def syncdb():
	'''Creates all missing tables'''
	db.create_all()

@manager.command
def newuser(username,password):
	'''Adds a new user'''
	from .models import User
	from hashlib import sha256
	m=sha256()
	m.update(password.encode())
	u=User(username=username,password=m.hexdigest())
	db.session.add(u)
	db.session.commit()

@manager.command
def passwd(username,password):
	from .models import User
	from hashlib import sha256
	a=get_user_spec(username)
	a.password=password
	db.session.commit()

@manager.command
def isadmin(username):
	from .models import User
	a=get_user_spec(username)
	a.admin=True
	db.session.commit()
