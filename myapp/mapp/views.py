from .app import app
from flask import render_template
from flask.ext.wtf import Form
import yaml, os.path
from wtforms import StringField, HiddenField, PasswordField, BooleanField,SelectField
from wtforms.validators import DataRequired
from flask import url_for, redirect,request
from .app import db
from hashlib import sha256
from flask.ext.login import login_user, current_user,logout_user,login_required

#ON IMPORT LES GETTER
from .models import get_all_genre,get_all_user,get_all_albums,get_all_playlist,get_all_chanteur,get_genre_spec,get_user_spec,get_albums_spec,get_playlist_spec,get_chanteur_spec,get_album,get_genre,add_genre,add_user,add_chanteur,User,get_playlist_user,add_playlist
from .models import get_artiste_albums,get_genre_albums,get_nom_genre,modif_genre,modif_album,modif_chanteur,suppr_album,get_chanteur,add_albums
from .models import recherche_albums,recherche_artistes,recherche_genres,add_album_playlist,get_album_playlist,suppr_album_playlist,suppr_playlist,recherche_playlist,modif_user

class LoginForm(Form):
	username=StringField('Pseudo')
	password=PasswordField('Mot de passe')
	next=HiddenField()

	def get_authenticated_user(self):
		user=get_user_spec(self.username.data)
		if user is None:
			return None
		m=sha256()
		m.update(self.password.data.encode())
		passwd=m.hexdigest()
		return user if passwd == user.password else None

class RegisterForm(Form):
	username= StringField('Pseudo', validators=[DataRequired()])
	password= PasswordField('Mot de passe')

class PlaylistForm(Form):
	nom = StringField('Nom', validators=[DataRequired()])
	description = StringField('Description')
	image=StringField('Image', validators=[DataRequired()])
	public=BooleanField('Privee : ')

class SearchForm(Form):
	element=StringField("Mots-clés",validators=[DataRequired()])

class SelectForm(Form):
	playlists = SelectField(u'Playlists disponibles : ',validators=[DataRequired()])

class addGenreForm(Form):
	genre=StringField(u'Nom du genre : ',validators=[DataRequired()])

class addAlbumForm(Form):
	nomss=StringField('Nom :',validators=[DataRequired()])
	chanteurss=StringField('Chanteur : ',validators=[DataRequired()])
	anneess=StringField('Année :',validators=[DataRequired()])
	imgss=StringField('Image : ',validators=[DataRequired()])
	genress=StringField('Genre :',validators=[DataRequired()])

class addChanteurForm(Form):
	chanteur=StringField(u"Nom du chanteur : ",validators=[DataRequired()])

class ProfileForm(Form):
	username= StringField('Pseudo')
	prevPassword= PasswordField('Ancien mot de passe')
	newPassword= PasswordField('Nouveau mot de passe')
	confPassword= PasswordField('Confirmez mot de passe')
	password= PasswordField('Mot de passe')

	def get_authenticated_user(self):
		user=get_user_spec(self.username.data)
		if user is None:
			return None
		m=sha256()
		m.update(self.password.data.encode())
		passwd=m.hexdigest()
		return user if passwd == user.password else None

class modifAlbumForm(Form):
	albums=SelectField(u'Album :',validators=[DataRequired()])
	nom=StringField('Nom :')
	chanteur=StringField('Chanteur : ')
	annee=StringField('Année :')
	genre=StringField('Genre :')

class modifGenreForm(Form):
	genre=SelectField(u'Genre :',validators=[DataRequired()])
	nom=chanteur=StringField('Nom : ')

class modifChanteurForm(Form):
	artistes=SelectField(u'Artiste :',validators=[DataRequired()])
	nom=chanteur=StringField('Nom : ')

@app.route("/")
def home():
	a=get_all_playlist()
	return render_template(
		"index.html",
		title="8Plays",
		donnees="Bienvenue sur le site pour gérer tes playlists !",playlists=a)

#LISTE GENERAL
#############
@app.route("/genres")
def genre():
	a = get_all_genre()
	return render_template(
		"genres.html",
		title = "Genres",
		donnees = a
	)

@app.route("/artistes")
def artistes():
	a = get_all_chanteur()
	return render_template(
		"artistes.html",
		title = "Artistes",
		donnees = a
	)

@app.route("/albums")
def albums():
	a = get_all_albums()
	return render_template(
		"albums.html",
		title = "Albums",
		donnees = a
	)

#SPECIFIQUE PAGE
################
@app.route("/genres/<int:id>")
def un_genre(id):
	a = get_genre_albums(id)
	return render_template(
		"genre.html",
		title = get_nom_genre(id).nom,
		albums = a
	)

@app.route("/artistes/<int:id>")
def un_artiste(id):
	a = get_artiste_albums(id)
	return render_template(
		"artiste.html",
		title = get_chanteur_spec(id).nom,
		artiste = a
	)

@app.route("/albums/<int:id>",methods=("GET","POST",))
def un_album(id):
	if current_user.is_authenticated:
		d=get_playlist_user(current_user.username)
	else:
		d=get_all_playlist()
	liste=[]
	for elem in d:
		liste.append((str(elem.id),elem.nom))
	b = SelectForm()
	a = get_album(id)
	b.playlists.choices=liste
	if b.validate_on_submit():
		add_album_playlist(a,int(b.playlists.data))
		return redirect(url_for("playlist",nom=current_user.username))
	return render_template(
		"album.html",
		title = a.nom,
		annee = a.annee,
		img= a.img,
		chanteur=get_chanteur_spec(a.idChanteur),
		form=b,
		id=a.id
		)

#GESTION USERS
##############

@app.route("/login", methods=("GET","POST",))
def login():
	f=LoginForm()
	if not f.is_submitted():
		f.next.data=request.args.get("next")
	elif f.validate_on_submit():
		user=f.get_authenticated_user()
		if user:
			login_user(user)
			next=f.next.data or url_for("home")
			return redirect(next)
	return render_template("login.html",form=f,title="Se connecter")

@app.route("/logout")
def logout():
	logout_user()
	return redirect(url_for("home"))

@app.route("/register",methods=("GET","POST",))
def register():
	try:
		r=RegisterForm()
		if r.validate_on_submit():
			from hashlib import sha256
			m=sha256()
			m.update(r.password.data.encode())
			add_user(r.username.data,m.hexdigest())
			return redirect(url_for("login"))
	except :
		print(u"Cet utilisateur existe déjà")
	return render_template("register.html",form=r,title="S'inscrire")


@app.route("/users/<string:username>",methods=("GET","POST",))
@login_required
def affiche_profil(username):
	p=ProfileForm()
	userLog=current_user.username
	user2=username
	if userLog == username:
		if 	p.validate_on_submit():
			from hashlib import sha256
			prev=sha256()
			prev.update(p.prevPassword.data.encode())
			pasw=get_user_spec(username)
			if prev.hexdigest()==pasw.password and p.newPassword.data==p.confPassword.data:
				m=sha256()
				m.update(p.newPassword.data.encode())
				modif_user(userLog,m.hexdigest())
		return render_template(
			"modif.html",
			form=p,
			title="Modifier profil",
			title2="Playlists",
			username=userLog,
			donnees=get_playlist_user(current_user.username))
	else:
		return render_template(
			"profil.html",
			title="Profil",
			title2="Playlists",
			username=user2,
			donnees=get_playlist_user(username)
	)


@app.route("/users")
def all_user():
	a = get_all_user()
	return render_template(
		"users.html",
		title = "Utilisateurs",
		donnees = a
)

@app.route("/playlist/users/<string:nom>",methods=("GET","POST",))
@login_required
def playlist(nom):
	a = get_playlist_user(nom)
	r=PlaylistForm()
	if r.validate_on_submit():
		add_playlist(r.nom.data,r.description.data,r.public.data,nom,r.image.data)
		return redirect(url_for("playlist",nom=nom))
	return render_template(
		"liste.html",
		title = "Mes playlists",
		donnees = a,
		form=r
		)
# Toutes les playlists de l'utilisateur indiqué

@app.route("/playlist/users/<string:nom>/supprimer/<int:id>")
def supprimeruneplaylis(nom,id):
	suppr_playlist(id)
	return redirect(url_for("playlist",nom=nom))

#AFFICHE UNE PLAYLIST SPECIFIQUE SELON SON ID
@app.route("/playlist/users/<int:id>")
def specplaylist(id):
	a=get_album_playlist(id)
	b=get_playlist_spec(id)
	return render_template("modif_playlist.html",donnees=a,playlist=b,user=b.nameUser)

@app.route("/playlist/users/supprimer/<int:id>/<int:album>")
@login_required
def suppralbumplaylist(id,album):
	if current_user.is_authenticated:
		b=get_playlist_spec(id)
		if current_user.username == b.nameUser:
			suppr_album_playlist(id,album)
	return redirect(url_for("playlist",nom=b.nameUser))

@app.route("/albums/supprimer/<int:id>")
@login_required
def suppralbum(id):
	if current_user.admin:
		suppr_album(id)
	return redirect(url_for("albums"))


@app.route("/recherche",methods=("GET","POST",))
def recherche():
	r=SearchForm()
	if r.validate_on_submit():
		a=r.element.data
		b=recherche_albums(a)
		c=recherche_artistes(a)
		d=recherche_genres(a)
		e=recherche_playlist(a)
		if not b:
			b=None
		if not c:
			c=None
		if not d:
			d=None
		if not e:
			e=None
		return render_template(
			"recherche.html",
			albums=b,
			artistes=c,
			genres=d,
			form=r,
			playlists=e,
			title="Recherche"
			)
	return render_template(
		"recherche.html",form=r,albums=None,
		artistes=None,
		genres=None,
		playlists=None,
		title="Recherche")

@app.route("/admin",methods=("GET","POST",))
@login_required
def administration():
	a=modifGenreForm()
	b=modifAlbumForm()
	c=modifChanteurForm()
	x=addGenreForm()
	y=addAlbumForm()
	z=addChanteurForm()

	add_genre,add_user,add_chanteur
	d=get_all_genre()
	e=get_all_albums()
	f=get_all_chanteur()

	liste1=[]
	for elem in d:
		liste1.append((str(elem.id),elem.nom))
	liste2=[]
	for elem in e:
		liste2.append((str(elem.id),elem.nom))
	liste3=[]
	for elem in f:
		liste3.append((str(elem.id),elem.nom))

	a.genre.choices=liste1
	b.albums.choices=liste2
	c.artistes.choices=liste3
	message=""
	if current_user.admin:
		if a.validate_on_submit():
			modif_genre(a.genre.data,a.nom.data)
			message="Genre modifié"
		elif b.validate_on_submit():
			modif_album(b.albums.data,b.nom.data,b.chanteur.data,int(b.annee.data))
			message="Album modifié"
		elif c.validate_on_submit():
			modif_chanteur(c.artistes.data,c.nom.data)
			message="Artiste modifié"
		elif x.validate_on_submit():
			add_genre(x.genre.data)
			message="Genre ajouté"
		elif z.validate_on_submit():
			add_chanteur(z.chanteur.data)
			message="Chanteur ajouté"
		elif y.validate_on_submit():
			chant=get_chanteur(y.chanteurss.data)
			if chant is None:
				add_chanteur(y.chanteurss.data)
			chant=get_chanteur(y.chanteurss.data).id
			listegenre=y.genress.data.split()
			for elem in listegenre:
				if get_genre(elem) is None:
					add_genre(elem)
			add_albums(y.nomss.data,y.anneess.data,y.imgss.data,chant,listegenre)
			message="Album ajouté"
		return render_template("admin.html",title="Administration",form1=a,form2=b,form3=c,form4=x,form5=y,form6=z,message=message)
	else:
		return redirect(url_for("home"))
# @app.route("/playlist/users/modifier/<int:id>")
# def modif_playlist(id):
#Modifier une playlist dont l'ID est indiquée et l'utilisateur indiqué doit etre connecté
