import yaml, os.path
from .app import db,manager,login_manager
from flask.ext.login import UserMixin
from  sqlalchemy.sql.expression import func

@login_manager.user_loader
def load_user(username):
    return User.query.get(username)

#TABLE DE LIAISON ENTRE PLAYLIST ET ALBUM
contient = db.Table('contient',
    db.Column('idPlaylist', db.Integer, db.ForeignKey('playlist.id')),
    db.Column('idAlbum', db.Integer, db.ForeignKey('album.id'))
)

#TABLE DE LIAISON ENTRE GENRE ET ALBUM
appartenir = db.Table('appartenir',
    db.Column('genre', db.Integer, db.ForeignKey('genre.id')),
    db.Column('idAlbum', db.Integer, db.ForeignKey('album.id'))
)

#TABLE USER
class User(db.Model, UserMixin):
    username= db.Column(db.String(50),primary_key=True)
    password = db.Column(db.String(64))
    admin=db.Column(db.Boolean)
    def get_id(self):
        return self.username

#TABLE CHANTEUR
class Chanteur(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	nom = db.Column(db.String(100))

	def get_nomc(self):
		return self.nom

	def __repr__(self):
		return "<Chanteur(id='%s', nom='%s'>"%(self.id,self.nom)

#TABLE DE GENRE
class Genre(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	nom = db.Column(db.String(100))

	def get_nomg(self):
		return self.nom

	def __repr__(self):
		return "<Genre(id='%s', nom='%s'>)"%(self.id,self.nom)

#TABLE ALBUM
class Album(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	nom = db.Column(db.String(100))
	annee = db.Column(db.Integer)
	img=db.Column(db.String(300))
	idChanteur = db.Column(db.Integer,db.ForeignKey("chanteur.id"))
	genre=db.relationship("Genre", secondary=appartenir,
		backref=db.backref("genreid",lazy="dynamic"))

	def get_noma(self):
		return self.nom

	def __repr__(self):
		return "<Album(id='%s', nom='%s', annee='%s', genre='%s')>"%(self.id,self.nom,self.annee,self.genre)

#TABLE DE PLAYLIST
class Playlist(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(100))
    description = db.Column(db.String(300))
    public=db.Column(db.Boolean)
    nameUser = db.Column(db.Integer,db.ForeignKey("user.username"))
    img=db.Column(db.String(300))
    albums=db.relationship("Album", secondary=contient,backref=db.backref("contenu",lazy="dynamic"))


#ENSEMBLE GETTER
################

#GETTER ALL
def get_all_genre():
	return Genre.query.order_by(Genre.nom.asc()).all()

def get_all_user():
	return User.query.all()

def get_all_albums():
	return Album.query.order_by(Album.nom.asc()).all()

def get_all_playlist():
	return Playlist.query.filter(Playlist.public==0).order_by(func.random()).limit(9).all()

def get_all_chanteur():
	return Chanteur.query.order_by(Chanteur.nom.asc()).all()

#GETTER SPECIFIC

def get_genre_spec(id):
	return Genre.query.get(id)

def get_user_spec(username):
	return User.query.get(username)

def get_albums_spec(id):
	return Album.query.get(id)

def get_playlist_spec(id):
	return Playlist.query.get(id)

def get_chanteur_spec(id):
	return Chanteur.query.get(id)

def get_chanteur(name):
    return Chanteur.query.filter_by(nom=name).first()

#GENERAL GET
############
def get_album_playlist(id):
    a=get_playlist_spec(id)
    liste=[]
    for elem in a.albums:
        liste.append(elem)
    return liste

#GETTER SPEC FILTER
###################

def get_album(id):
	return Album.query.filter_by(id = id).first()

def get_album_by_name(name):
    return Album.query.filter_by(nom=name).first()

def get_genre(name):
	return Genre.query.filter_by(nom = name).first()

def get_playlist_user(username):
    return Playlist.query.filter_by(nameUser=username).all()

def get_artiste_albums(id):
    return Album.query.filter_by(idChanteur=id).all()

def get_genre_albums(id):
    return Album.query.filter(Album.genre.any(id=id)).all()

def get_nom_genre(id):
    return Genre.query.filter_by(id=id).first()

#ELEMENTS RECHERCHE
###################
def recherche_albums(mot):
    return Album.query.filter(Album.nom.like("%"+mot+"%")).all()

def recherche_artistes(mot):
    return Chanteur.query.filter(Chanteur.nom.like("%"+mot+"%")).all()

def recherche_genres(mot):
    return Genre.query.filter(Genre.nom.like("%"+mot+"%")).all()

def recherche_playlist(mot):
    return Playlist.query.filter(Playlist.nom.like("%"+mot+"%")).limit(10).all()
#SUPPRESSION QUERY
##################

def suppr_album_playlist(id,album):
    a=get_albums_spec(album)
    b=get_playlist_spec(id)
    if a in b.albums:
        b.albums.remove(a)
    db.session.add(b)
    db.session.commit()

def suppr_playlist(id):
    a=get_playlist_spec(id)
    db.session.delete(a)
    db.session.commit()

def suppr_album(id):
    a=get_albums_spec(id)
    db.session.delete(a)
    db.session.commit()
#ENSEMBLE ADDER
###############

def add_genre(nomg):
	g=Genre(nom=nomg)
	db.session.add(g)
	db.session.commit()

def add_user(nomu,passu):
	g=User(username=nomu,password=passu)
	db.session.add(g)
	db.session.commit()

def add_albums(noma,anneea,imga,idChanteura,genrea):
    print(noma)
    print(anneea)
    print(imga)
    print(idChanteura)
    print(genrea)
    a=Album(nom=str(noma),annee=int(anneea),img=str(imga),idChanteur=int(idChanteura))
    for elem in genrea:
        g=get_genre(elem)
        a.genre.append(g)
    db.session.add(a)
    db.session.commit()

def add_chanteur(nomc):
	g=Chanteur(nom=nomc)
	db.session.add(g)
	db.session.commit()

def add_playlist(nomp,descp,priv,username,image):
    g=Playlist(nom=nomp,description=descp,public=priv,nameUser=username,img=image)
    db.session.add(g)
    db.session.commit()

def add_album_playlist(album,id):
    g=get_playlist_spec(id)
    g.albums.append(album)
    db.session.add(g)
    db.session.commit()

def modif_user(username,modif):
    g=get_user_spec(username)
    g.password=modif
    db.session.commit()

def modif_genre(pilote,nom):
    g=get_genre_spec(pilote)
    g.nom=nom
    db.session.commit()

def modif_chanteur(pilote,nom):
    g=get_chanteur_spec(pilote)
    g.nom=nom
    db.session.commit()

#modif_album(albums.data,nom.data,chanteur.data,int(annee.data))
def modif_album(pilote,nom,chanteur,date):
    g=get_album_by_name(pilote)
    g.nom=nom
    if get_chanteur(chanteur) != None:
        e=get_chanteur(chanteur)
    else:
        e=User(nom=chanteur)
        db.session.commit()
    g.idChanteur=e.id
    g.annee=int(date)
    db.session.commit()
