#Pour installer un virtual env avec python 3 (linux) :
  >virtualenv -p python3 venv

#Activer le virtual env :
  > source venv/bin/activate

#Ensuite installer les composants suivants :

**Avec pip (pip install ...)**

  - flask
  - flask-script
  - flask-wtf
  - flask-login
  - flask-sqlalchemy
  - flask-bootstrap
  - pyyaml
